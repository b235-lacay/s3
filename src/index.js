const express = require("express");
const app = express();
const port = 4000;

app.use(express.json());

let users = [
 {
 	name: "Jojo joestar",
 	age: 25,
 	username: "Jojo"
 },
 {
 	name: "Dio Brando",
 	age: 23,
 	username: "Dio"	
 },
 {
 	name: "Jotaro Kujo",
 	age: 28,
 	username: "Jotaro"
 }
]


 // [SECTION]ROUTES and CONTROLLERS for USERS

 app.get("/users", (req,res) => {
 	return res.send(users);
 });
 // add simple if statement that if the request body does not havve property name, we will send message along with a 400 http status code(Bad request)
 // hasOwnProperty() returns a bool if the property name passed exists or does not exist in the given object
 app.post("/users", (req,res) => {
    if(!req.body.hasOwnProperty("name")){
        return res.status(400).send({
            error: "Bad request - missing required parameter NAME"
        })
    }
    if(!req.body.hasOwnProperty("age")){
        return res.status(400).send({
            error: "Bad request - missing required parameter AGE"
        })
    }
 })

 // [SECTION] ACTIVITY
let artists = [
 {
    name: "Marlo Mortel",
    songs: ["I pray", "Tado"],
    album: "Tado",
    isActive: true
 },
 {
    name: "BTS",
    songs: ["Butter", "Dynamite"],
    album: "Butter",
    isActive: true
 },
 {
    name: "Adie",
    songs: ["Mahika", "Paraluman"],
    album: "Mahika",
    isActive: true
 }
]
// [SECTION]ROUTES and CONTROLLERS for USERS

 app.get("/artists", (req,res) => {
    return res.send(artists);
 });

app.post("/artists", (req,res) => {
    if(!req.body.hasOwnProperty("name")){
        return res.status(400).send({
            error: "Bad request - missing required parameter NAME"
        })
    }
    if(!req.body.hasOwnProperty("songs")){
        return res.status(400).send({
            error: "Bad request - missing required parameter SONGS"
        })
    }
    if(!req.body.hasOwnProperty("album")){
        return res.status(400).send({
            error: "Bad request - missing required parameter ALBUM"
        })
    }
    if(!req.body.hasOwnProperty("isActive")){
        return res.status(400).send({
            error: "Bad request - missing required parameter isActive"
        })
    }
 })













// Tells our server to listen to the port
app.listen(port, () => console.log(`server running at port ${port}`));
